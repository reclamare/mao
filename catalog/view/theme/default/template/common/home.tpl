<?php echo $header; ?>
<div class="container">
    <div class="row">

        <?php echo $column_left; ?>
        <?php if ($column_left && $column_right) { ?>
        <?php $class = 'col-sm-6'; ?>
        <?php } elseif ($column_left || $column_right) { ?>
        <?php $class = 'col-sm-9'; ?>
        <?php } else { ?>
        <?php $class = 'col-sm-12'; ?>
        <?php } ?>
        <div id="content" class="<?php echo $class; ?>"><?php echo $content_top; ?>
            <div class="row">
                <div class="col-xs-12">
                    <div class="catalog-homepage-links">
                        <div class="row">
                            <div class="col-xs-6 col-sm-4 col-md-3 catalog-homepage-links-grid">
                                <a href="#" class="catalog-link" role="link">
                                    <span class="image"
                                          style="background-image: url(./catalog/view/theme/default/image/car1.png)"></span>
                  <span class="title">
                    Gelly mk
                  </span>
                                </a>
                            </div>
                            <div class="col-xs-6 col-sm-4 col-md-3 catalog-homepage-links-grid">
                                <a href="#" class="catalog-link" role="link">
                                    <span class="image"
                                          style="background-image: url(./catalog/view/theme/default/image/car2.png)"></span>
                  <span class="title">
                    Gelly mk
                  </span>
                                </a>
                            </div>
                            <div class="col-xs-6 col-sm-4 col-md-3 catalog-homepage-links-grid">
                                <a href="#" class="catalog-link" role="link">
                                    <span class="image"
                                          style="background-image: url(./catalog/view/theme/default/image/car3.png)"></span>
                  <span class="title">
                    Gelly mk
                  </span>
                                </a>
                            </div>
                            <div class="col-xs-6 col-sm-4 col-md-3 catalog-homepage-links-grid">
                                <a href="#" class="catalog-link" role="link">
                                    <span class="image"
                                          style="background-image: url(./catalog/view/theme/default/image/car4.png)"></span>
                  <span class="title">
                    Gelly mk
                  </span>
                                </a>
                            </div>
                            <div class="col-xs-6 col-sm-4 col-md-3 catalog-homepage-links-grid">
                                <a href="#" class="catalog-link" role="link">
                                    <span class="image"
                                          style="background-image: url(./catalog/view/theme/default/image/car5.png)"></span>
                  <span class="title">
                    Gelly mk
                  </span>
                                </a>
                            </div>
                            <div class="col-xs-6 col-sm-4 col-md-3 catalog-homepage-links-grid">
                                <a href="#" class="catalog-link" role="link">
                                    <span class="image"
                                          style="background-image: url(./catalog/view/theme/default/image/car6.png)"></span>
                  <span class="title">
                    Gelly mk
                  </span>
                                </a>
                            </div>
                            <div class="col-xs-6 col-sm-4 col-md-3 catalog-homepage-links-grid">
                                <a href="#" class="catalog-link" role="link">
                                    <span class="image"
                                          style="background-image: url(./catalog/view/theme/default/image/car7.png)"></span>
                  <span class="title">
                    Gelly mk
                  </span>
                                </a>
                            </div>
                            <div class="col-xs-6 col-sm-4 col-md-3 catalog-homepage-links-grid">
                                <a href="#" class="catalog-link" role="link">
                                    <span class="image"
                                          style="background-image: url(./catalog/view/theme/default/image/car11.jpg)"></span>
                  <span class="title">
                    Gelly mk
                  </span>
                                </a>
                            </div>
                            <div class="col-xs-6 col-sm-4 col-md-3 catalog-homepage-links-grid">
                                <a href="#" class="catalog-link last" role="link">
                                    <span class="image"
                                          style="background-image: url(./catalog/view/theme/default/image/car11.jpg)"></span>
                  <span class="title">
                    Gelly mk
                  </span>
                                </a>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
            <?php echo $content_bottom; ?>
            <div class="html-block-homepage">

                <div class="row">
                    <div class="col-xs-12 col-lg-5 col-lg-push-7 html-text-image">
                        <img src="./catalog/view/theme/default/image/geely-seo.png" alt="" class="img-responsive">
                    </div>
                    <div class="col-xs-12 col-sm-10 col-sm-push-1  col-lg-6 col-lg-pull-6 col-lg-push-1 text-left-md">
                        <div class="h2">
                            Запчасти Geely Джили - купить в Киеве
                        </div>
                        <p>
                            Компания Geely — одна из крупнейших автомобилестроительных компаний Китая, которая была
                            основана в 1986 году. Стремительно набирая популярность и сегодня, она предоставляет
                            качественные и надежные запчасти для Geely среди многих покупателей в Киеве и на всей
                            территории Украины.
                        </p>
                        <p>
                            Несмотря на то, что свое первое детище компания выпустила только в 1998 году, на запчасти
                            Джили с тех пор большой спрос не только в Китае, но и во многих городах и странах мира.
                        </p>
                    </div>



                </div>
                <div class="row">
                    <div class="col-xs-12 col-sm-10 col-sm-push-1">

                        <div class="h2"> Как выбрать запчасти для Geely?</div>
                        <p>
                            Замена запчастей для владельцев автомобилей — обязательная необходимость. В особенности не
                            на самых лучших дорогах. Поэтому на сайте нашего магазина автозапчастей для китайских
                            автомобилей «МАО» вы сможете выбрать и заказать необходимые детали для вашего Джили без
                            особых усилий. Не нужно переплачивать за товар и тратить свое время, разъезжая по городу.
                            Достаточно просто следовать простым инструкциям и оформить заказ одним кликом. А по всем
                            необходимым вопросам наши специалисты всегда доступны в режиме онлайн и помогут с вашим
                            выбором. Несмотря на то, что свое первое детище компания выпустила только в 1998 году, на
                            запчасти Джили с тех пор большой спрос не только в Китае, но и во многих городах и странах
                            мира.
                            В 2013 году компания заняла топовое место среди самых лучших азиатских компаний по версии
                            Forbes.
                        </p>
                    </div>

                </div>
                <div class="row happy-customer">
                    <div class="col-xs-7 col-sm-6 col-md-5 col-lg-4 col-lg-push-1">
                        <img src="./catalog/view/theme/default/image/happy_customer.png" alt="" class="img-responsive">
                    </div>
                    <div class="col-xs-5 col-sm-6 col-md-6 col-md-pull-1 col-lg-6 happy-customer-text">
                        <div class="happy-customer-title">
                            довольных клиента
                        </div>
                        <div class="happy-customer-number">
                            6548
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<?php echo $footer; ?>